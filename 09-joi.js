const Joi = require('joi');

const jsonPersona = {
    nombre: Joi.string().alphanum().min(3).max(30).required(),
    apellido: Joi.string().alphanum().min(3).max(30).required(),
    edad: Joi.number().integer().min(1900).max(2013),
}

const jsonValidacionPersona = Joi
                                .object()
                                .keys(jsonPersona)
                                .with('nombre', 'apellido');

const jsonPrueba = {
    nombre: 'Carlos',
    apellido: 'Ayala'
}
Joi.validate(jsonPrueba, jsonValidacionPersona, (err, valor) => {
    console.log(err)
    console.log(valor)
});