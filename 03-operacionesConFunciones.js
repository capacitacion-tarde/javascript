

var obj = {
    nombre: 'kevin'
}


var funcionesBasicas = {
    suma: (x, y) => {
        return x + y
    },

    resta: (x, y) => {
        return x - y
    },

    division: (x, y) => {
        return x / y
    },

    multi: (x, y) => {
        return x * y
    },

    modulo: (x, y) => {
        return x % y
    }
}




/**
 * esta funcion suma
 * @param {number} x un numero
 * @param {*}  * un cualquiera
 * 
 * @returns {number} el output es un numero
 */
var suma = (x, y) => {
    return x + y
}

/**
 * esta funcion resta
 * @param {number} 
 * @access public
 * @alias restFuntion 
 * @augments x
 * @augments y
 * @borrows trstr as trim
 * 
 */
var resta = (x, y) => {
    return x - y
}

/**
 * esta funcion divide
 * @param {number} 
 * 
 */
var division = (x, y) => {
    return x / y
}

/**
 * esta funcion multiplica
 * @param {number} 
 * @lends 
 * 
 */
var multi = (x, y) => {
    return x * y
}

/**
 * esta funcion saca el mudulo
 * @param {number} 
 * 
 */
var modulo = (x, y) => {
    return x % y
}






/// SUMA

console.log(`suma entre un number y (number,string,null, undefined, boolean, object)`)
console.log(funcionesBasicas.suma(5, 5))
console.log(funcionesBasicas.suma(5, 'd'))
console.log(funcionesBasicas.suma(5, null))
console.log(funcionesBasicas.suma(5, undefined))
console.log(funcionesBasicas.suma(5, true))
console.log(funcionesBasicas.suma(5, obj))

console.log(funcionesBasicas.suma(true, 5))
console.log(funcionesBasicas.suma(false, 'd'))
console.log(funcionesBasicas.suma(true, null))
console.log(funcionesBasicas.suma(false, undefined))
console.log(funcionesBasicas.suma(true, false))
console.log(funcionesBasicas.suma(false, obj))

console.log(funcionesBasicas.suma(undefined, 5))
console.log(funcionesBasicas.suma(undefined, 'd'))
console.log(funcionesBasicas.suma(undefined, null))
console.log(funcionesBasicas.suma(undefined, undefined))
console.log(funcionesBasicas.suma(undefined, false))
console.log(funcionesBasicas.suma(undefined, obj))

console.log(funcionesBasicas.suma(null, 5))
console.log(funcionesBasicas.suma(null, 'd'))
console.log(funcionesBasicas.suma(null, null))
console.log(funcionesBasicas.suma(null, undefined))
console.log(funcionesBasicas.suma(null, false))
console.log(funcionesBasicas.suma(null, obj))

console.log(funcionesBasicas.suma('null', 5))
console.log(funcionesBasicas.suma('null', 'd'))
console.log(funcionesBasicas.suma('null', null))
console.log(funcionesBasicas.suma('null', undefined))
console.log(funcionesBasicas.suma('null', false))
console.log(funcionesBasicas.suma('null', obj))


console.log(funcionesBasicas.suma(obj, 5))
console.log(funcionesBasicas.suma(obj, 'd'))
console.log(funcionesBasicas.suma(obj, null))
console.log(funcionesBasicas.suma(obj, undefined))
console.log(funcionesBasicas.suma(obj, false))
console.log(funcionesBasicas.suma(obj, obj))




///// RESTA
console.log(funcionesBasicas.resta(5, 5))
console.log(funcionesBasicas.resta(5, 'd'))
console.log(funcionesBasicas.resta(5, null))
console.log(funcionesBasicas.resta(5, undefined))
console.log(funcionesBasicas.resta(5, true))
console.log(funcionesBasicas.resta(5, obj))
console.log(funcionesBasicas.resta(null, null))
console.log(funcionesBasicas.resta(null, undefined))
console.log(funcionesBasicas.resta(undefined, undefined))
console.log(funcionesBasicas.resta(true, undefined))
console.log(funcionesBasicas.resta(null, true))
console.log(funcionesBasicas.resta('f', null))
console.log(funcionesBasicas.resta('f', undefined))
console.log(funcionesBasicas.resta(obj, null))
console.log(funcionesBasicas.resta(obj, undefined))
console.log(funcionesBasicas.resta(obj, obj))


/// MULTI
console.log(funcionesBasicas.multi(5, 5))
console.log(funcionesBasicas.multi(5, 'd'))
console.log(funcionesBasicas.multi(5, null))
console.log(funcionesBasicas.multi(5, undefined))
console.log(funcionesBasicas.multi(5, true))
console.log(funcionesBasicas.multi(5, obj))
console.log(funcionesBasicas.multi(null, null))
console.log(funcionesBasicas.multi(null, undefined))
console.log(funcionesBasicas.multi(undefined, undefined))
console.log(funcionesBasicas.multi(true, undefined))
console.log(funcionesBasicas.multi(null, true))
console.log(funcionesBasicas.multi('f', null))
console.log(funcionesBasicas.multi('f', undefined))
console.log(funcionesBasicas.multi(obj, null))
console.log(funcionesBasicas.multi(obj, undefined))
console.log(funcionesBasicas.multi(obj, obj))



/// DIVISION
console.log(funcionesBasicas.division(5, 5))
console.log(funcionesBasicas.division(5, 'd'))
console.log(funcionesBasicas.division(5, null))
console.log(funcionesBasicas.division(5, undefined))
console.log(funcionesBasicas.division(5, true))
console.log(funcionesBasicas.division(5, obj))
console.log(funcionesBasicas.division(null, null))
console.log(funcionesBasicas.division(null, undefined))
console.log(funcionesBasicas.division(undefined, undefined))
console.log(funcionesBasicas.division(true, undefined))
console.log(funcionesBasicas.division(null, true))
console.log(funcionesBasicas.division('f', null))
console.log(funcionesBasicas.division('f', undefined))
console.log(funcionesBasicas.division(obj, null))
console.log(funcionesBasicas.division(obj, undefined))
console.log(funcionesBasicas.division(obj, obj))


/// MODULO
console.log(funcionesBasicas.modulo(5, 5))
console.log(funcionesBasicas.modulo(5, 'd'))
console.log(funcionesBasicas.modulo(5, null))
console.log(funcionesBasicas.modulo(5, undefined))
console.log(funcionesBasicas.modulo(5, true))
console.log(funcionesBasicas.modulo(5, obj))
console.log(funcionesBasicas.modulo(null, null))
console.log(funcionesBasicas.modulo(null, undefined))
console.log(funcionesBasicas.modulo(undefined, undefined))
console.log(funcionesBasicas.modulo(true, undefined))
console.log(funcionesBasicas.modulo(null, true))
console.log(funcionesBasicas.modulo('f', null))
console.log(funcionesBasicas.modulo('f', undefined))
console.log(funcionesBasicas.modulo(obj, null))
console.log(funcionesBasicas.modulo(obj, undefined))
console.log(funcionesBasicas.modulo(obj, obj))
