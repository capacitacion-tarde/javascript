const moment = require('moment')

var jsonFecha = {
    year: 2018,
    month: 7,
    day: 25
}

var fecha = moment(jsonFecha).format('YYYY-MM-DD')
console.log('fecha', fecha)

var fechaInicial = moment()

var fechaFinal = moment('2018-12-24')

var diferencia = console.log('diff', fechaFinal.diff(fechaInicial,'days'))




// array de objetos con un atributo nombre, al menos 10 nombres, 
// mapear y agregar el atriuto fecha de nacimiento de forma randomica en dos rangos 
// 1940 2018, calcular la edad, separar en tres respuestas quienes son mayores 18-55, 
// menores y tercera  



var arregloNombres = [
    {
        nombre: 'Edwin'
    },
    {
        nombre: 'Kevin'
    },
    {
        nombre: 'Alexandra'
    },
    {
        nombre: 'Andrea'
    },
    {
        nombre: 'Fabricio'
    },
    {
        nombre: 'Carlos'
    },
    {
        nombre: 'Paul'
    },
    {
        nombre: 'Daniela'
    },
    {
        nombre: 'Karen'
    },
    {
        nombre: 'Miguel'
    },
]



var fechaValida = () => {
    var fechaNacimiento = moment().set({
        'year': Math.floor(Math.random() * (2018 - 1940) + 1940), 
        'month': Math.floor(Math.random() * (12 - 1) + 1), 
        'date': Math.floor(Math.random() * (31 - 1) + 1)})
        .format('YYYY-MM-DD');
    return fechaNacimiento
}

console.log(fechaValida());
var calcularEdades =(fechaNacimiento)=>{
    return moment().diff(fechaNacimiento,'years')
  }

var agregarEdades = arregloNombres.map((valor)=>{
    valor.fechaNacimiento = fechaValida()
    valor.edad = calcularEdades(valor.fechaNacimiento)
    return valor
})

console.log(agregarEdades)
