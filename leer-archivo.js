const fs = require('fs')


module.exports = (path,cb, info) => {
    
    fs.readFile(path, 'utf8', (errorLectura, contenidoArchivo) => {
        
        if(errorLectura) {
            fs.writeFile(path, info, (errorCreacion) => {
                if(errorCreacion) {
                    cb({
                        error:errorCreacion,
                        nombreArchivo: path,
                        informacionNueva:info,
                        archivoCreado:false
                    })
                } else {
                    cb({
                        error:errorCreacion,
                        nombreArchivo: path,
                        informacionNueva:info,
                        archivoCreado:true
                    })
                }
                
            })
            
            
        } else {
            cb({
                error:errorLectura,
                informacion:contenidoArchivo,
                tieneError: false
            })   
        }
    });

}
