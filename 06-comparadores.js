/* and 
or 
equals 
=
== // compara solo el valor, contenido
=== // compara el valor y el tipo de variable */

var iteradorMenosUno = 100
var condicionValorActual1 = iteradorMenosUno>0
console.log("Iterar menos 1 por bucle")

while (condicionValorActual1) {
    let valorMitad = iteradorMenosUno == 50
    let valorIgualDiez = iteradorMenosUno == 10
    let valorIgualUno = iteradorMenosUno == 1
    if (valorMitad) console.log("bucle esta por la mitad");
    else if (valorIgualDiez) console.log("bucle esta por terminar");
    else if (valorIgualUno) console.log("bucle termino");
    else  console.log(iteradorMenosUno);
    
    iteradorMenosUno--
    condicionValorActual1 = iteradorMenosUno>0
    
}


console.log("\nIterar menos 4 por bucle")

var iteradorMenosCuatro = 100
var condicionValorActual2 = iteradorMenosCuatro>0
while (condicionValorActual2) {
    let valorPorMitad = iteradorMenosCuatro >= 50 && iteradorMenosCuatro <= 52
    let valorPorDiez = iteradorMenosCuatro >= 10 && iteradorMenosCuatro <= 12
    let valorPorTerminar = iteradorMenosCuatro <= 4
    if (valorPorMitad) console.log("bucle esta por la mitad");
    else if (valorPorDiez) console.log("bucle esta por terminar");
    else if (valorPorTerminar) console.log("bucle termino");
    else console.log(iteradorMenosCuatro);
    
    iteradorMenosCuatro=iteradorMenosCuatro-4
    condicionValorActual2 = iteradorMenosCuatro>0   
}