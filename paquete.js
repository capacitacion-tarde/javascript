const saludar = require('./saludar.js')
const despedir = require('./despedir')
const sumar = require('./sumar.js')
const restar = require('./restar.js')
const multiplicar = require('./multiplicar.js')

module.exports = {
    saludar,
    despedir,
    sumar,
    restar, 
    multiplicar
}