const EventEmitter = require('events');


class EntradaALaCasa extends EventEmitter{}

const entrarALaCasa = new EntradaALaCasa();

// crear el escucha
entrarALaCasa.on('entro por la puerta delantera', (nombre) => {
    console.log(`saludar a ${nombre}, que entro por la puerta delantera`)
})

entrarALaCasa.on('entro por la puerta delantera', (nombre) => {
    console.log(`saludar a ${nombre}, que ingreso por la puerta trasera`)
})

entrarALaCasa.on('entro por la ventana', () => {
    console.log(`cuidado alguien entro por la ventana`)
})



entrarALaCasa.emit('entro por la puerta delantera', 'Carlos')
entrarALaCasa.emit('entro por la puerta trasera', 'Carlos')
entrarALaCasa.emit('entro por la ventana')


class BuscarObjeto extends EventEmitter{};
const buscarObjeto = new BuscarObjeto();


buscarObjeto.on('encontro objeto', (objeto) => {
    console.log(`encontro ${objeto}, dentro del cuarto`)
})

buscarObjeto.on('no encontro objeto', (objeto) => {
    console.log(`no encontro ${objeto}, dentro del cuarto`)
})
buscarObjeto.on('no existe objeto', () => {
    console.log(`no existe objeto`)
})

buscarObjeto.emit('encontro objeto', 'celular')
buscarObjeto.emit('no encontro objeto', 'celular')
buscarObjeto.emit('no existe objeto')



