// crear

module.exports.crearUsuario = (arregloUsuarios, usuarioNuevo, cb) => {
    arregloUsuarios.push(usuarioNuevo)
    cb({
        mensaje: 'usuario creado exitosamente',
        usuarios: arregloUsuarios
    })
}

// buscar

module.exports.buscarUsuario = (arregloUsuarios,usuarioABuscar, cb) => {
    const usuarioEncontrado = arregloUsuarios.find((usuario) => {
        return usuario === usuarioABuscar
    })
    
    if(usuarioEncontrado) {
        cb({
            mensaje:'usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    } else {
        cb({
            mensaje:'usuario no encontrado',
            usuarioEncontrado
        })
    }
    

}


// buscarCrear
module.exports.crearBuscarUsuario = (arregloUsuarios,usuarioABuscarOCrear,cb)=>{
    buscarUsuario(arregloUsuarios,usuarioABuscarOCrear,(respuestaBuscar)=>{
    
        if(respuestaBuscar.usuarioEncontrado){
            cb({
                mensaje:'se encontro el usuario',
                usuarioEncontrado: respuestaBuscar.usuarioEncontrado
            })
        }
        else{
            crearUsuario(arregloUsuarios,usuarioABuscarOCrear,(respuestaCrearUsuario)=>{
                cb({
                    mensaje:'usuario creado',
                    arregloUsuarios
                })
            })
        }
    })
}


//eliminar 

// eliminar
module.exports.eliminarUsuario = (arregloUsuarios, usuarioEliminar, cb) => {
    usuarioEliminar.pop(usuarioEliminar)
    cb({
        mensaje: 'usuario eliminado exitosamente',
        usuarios: arregloUsuarios
    })
}

//buscarEliminar
module.exports.buscarEliminarUsuario = (arregloUsuarios, usuarioABuscarEliminar, cb) => {
    buscarUsuario(arregloUsuarios,usuarioABuscarEliminar, (respuestaBuscar) => {
        if(respuestaBuscar.usuarioEncontrado){
            eliminarUsuario(arregloUsuarios, usuarioABuscarEliminar, (respuestaEliminarUsuario) => {
                cb({
                    mensaje:'usuario eliminado',
                    arregloUsuarios
                })
            })
        }
        else{
            cb({
                mensaje:'usuario no encontrado',
                arregloUsuarios
            })
        }
    })
}