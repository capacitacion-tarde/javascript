var arregloNumeros = [1,2,3,4,5]

console.log(arregloNumeros.length)

console.log(arregloNumeros.indexOf(2))

console.log(arregloNumeros.push(6))

console.log(arregloNumeros[3])

// métodos para clonar: slice y splice
console.log('original ', arregloNumeros)
var arregloSlice = arregloNumeros.slice(0,3)
console.log(arregloSlice)

/* var arregloSplice = arregloNumeros.splice(0,2)
console.log(arregloSplice) */

// pop
console.log('original despues ', arregloNumeros)

console.log(arregloNumeros.pop())
console.log('original despues del pop ', arregloNumeros)

// foreach ---> para recorrer
/* arregloNumeros.forEach(function(valor,indice, arreglo) {
    console.log(`valor: ${valor}`)
    console.log(`indice: ${indice}`)
    console.log(`arreglo: ${arreglo}`)
}); */

arregloNumeros.forEach((valor,indice, arreglo) =>{
    console.log(`valor: ${valor}`)
    console.log(`indice: ${indice}`)
    console.log(`arreglo: ${arreglo}`)
});


// map devuelve un arreglo con los cambios
var operadorMap = arregloNumeros.map((valor,indice) => {
    if (indice%2 ===0) valor += 7;
    return valor
})

console.log(arregloNumeros)
console.log(operadorMap)

// buscar elementos dentro de un vector: find y filter

var operadorFilter = arregloNumeros.filter((valor,indice) => {
    return valor === 3
})

console.log(arregloNumeros)
console.log(operadorFilter)

var operadorFind = arregloNumeros.find((valor,indice) => {    // similar al indexOf
    return valor === 3
})

console.log(arregloNumeros)
console.log(operadorFind)

