var a = {
  nombre: "Carlos",
  edad: 100,
  soltero: true,
  fechaNacimiento: new Date("1995-10-03")
 }
 
 
 console.log(a.nombre)
 console.log(a["edad"])
 
 a.nombre = "nuevo valor"
 console.log(a.nombre)
 
 a.direccion = "La Tola"
 
 console.log(a)
 
 delete a.direccion
 
 console.log(a)

// ------------------

var familia={
    ecuatorianos: true,
    persona1: [
      {
        miembro: "padre",
        nombre: "Marcelino",
        edad: 44
      }
    ],
    persona2: [
      {
        miembro: "madre",
        nombre: "Narciza",
        edad: 40
      }
    ],
    persona3: [
      {
        miembro: "Hijo",
        nombre: "Carlos",
        edad: 22
      }
    ],
    persona4: [
      {
        miembro: "Hija",
        nombre: "Michelle",
        edad: 21
      }
    ]
  }

console.log(familia)

var a = {
  nombre: "Carlos",
  edad: 100,
  soltero: true,
  fechaNacimiento: new Date("1995-10-03")
 }

var objetoString = JSON.stringify(a)

var b = JSON.parse(JSON.stringify(a))

b.nombre ="valor de b"
console.log(a)
console.log(b)

var a = 4
var b = "3"

var operacion = a+b
console.log(a+b)


var c = {
  num : 3
}
var d =true
var e = {
  num : 4
}

var f = ["s","a"]
console.log(c+f)
console.log(typeof(c+f))




// pruebaaa

var padre={
  ecuatorianos: true,
  datosPersonales:
    {
      nombre: "Marcelino",
      edad: 44
    }
}

var madre={
  ecuatorianos: true,
  datosPersonales:
    {
      nombre: "Narciza",
      edad: 40
    }
}

console.log(padre.datosPersonales.edad + madre.datosPersonales.edad)

var hijo = JSON.parse(JSON.stringify(padre))

hijo.datosPersonales.nombre = "Carlos"
hijo.datosPersonales.edad = 22
console.log(hijo)
console.log(padre.datosPersonales.edad + hijo.datosPersonales.edad)

var hija={
  ecuatorianos: true,
  datosPersonales: [
    {
      nombre: "Michelle",
      edad: 21
    },{
      edad2: 20
    }
  ]
}

console.log(hija.datosPersonales[1].edad2+hijo.datosPersonales.edad)

console.log("Familia: " + padre.datosPersonales.nombre + ", " + madre.datosPersonales.nombre + ", " + hijo.datosPersonales.nombre + ", " + hija.datosPersonales[0].nombre)

// segunda forma y mejor para concatenar

var familia = `Familia: ${padre.datosPersonales.nombre}, ${madre.datosPersonales.nombre}, ${hijo.datosPersonales.nombre}, ${hija.datosPersonales[0].nombre}`
console.log(familia)